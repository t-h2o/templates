#!/bin/sh

artist=""
album=""
year=""
title=""
track=""

echo_id3 () {
	echo id3tag --artist "\"${artist}\"" \
		--album "\"${album}\"" \
		--year "\"${year}\"" \
		--track "\"${track}\"" \
		--song "\"${title}\"" "\"${1}\""
}

tag_id3 () {
	 id3tag --artist "${artist}" \
		--album "${album}" \
		--year "${year}" \
		--track "${track}" \
		--song "${title}" "${1}"
}

for m4a in *\.m4a
do
	# Get media informations
	album="$(mediainfo --Output=JSON "${m4a}" | jq '.media.track[0].Album' | awk '{print substr($0,2,length($0)-2)}')"
	title="$(mediainfo --Output=JSON "${m4a}" | jq '.media.track[0].Title' | awk '{print substr($0,2,length($0)-2)}')"
	artist="$(mediainfo --Output=JSON "${m4a}" | jq '.media.track[0].Performer' | awk '{print substr($0,2,length($0)-2)}')"
	track="$(mediainfo --Output=JSON "${m4a}" | jq '.media.track[0].Track_Position' | awk '{print substr($0,2,length($0)-2)}')"
	
	# Print collected data
	echo ""
	echo " Album: ${album}"
	echo " Title: ${title}"
	echo "Artist: ${artist}"
	echo " Track: ${track}"
	echo "  Year: ${year}"
	echo ""

	# Change the extension name
	mp3="${m4a::-4}.mp3"

	# Convert from m4a into mp3
	echo ""
	echo ffmpeg -i "${m4a}" "${mp3}"
	echo ""
	#ffmpeg -i "${m4a}" "${mp3}"

	# Tag the file
	echo ""
	echo_id3 "${mp3}"
	echo ""
	#tag_id3 "${mp3}"

	echo "################################################################################"
done
